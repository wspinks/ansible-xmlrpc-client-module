from datetime import datetime

from twisted.web import xmlrpc, server
from twisted.internet import reactor

class TestServer(xmlrpc.XMLRPC):
    """An example object to be published."""

    def xmlrpc_date(self):
        return datetime.now()

    def xmlrpc_echo(self, x):
        """
        Return all passed args.
        """
        print('Echo: {var}'.format(
            var=x
        ))
        return x

    def xmlrpc_add(self, a, b):
        """
        Return sum of arguments.
        """
        return a + b

    def xmlrpc_fault(self):
        """
        Raise a Fault indicating that the procedure should not be used.
        """
        raise xmlrpc.Fault(123, "The fault procedure is faulty.")


if __name__ == '__main__':
    r = TestServer()
    reactor.listenTCP(7080, server.Site(r))
    reactor.run()

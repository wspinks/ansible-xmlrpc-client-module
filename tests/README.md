# Testing

With xmlrpc\_client.py in your library path and being able to login to localhost over ssh you can run the xmlrpc\_server.py script in one terminal like this;

	$ python xmlrpc_server.py

It will listen on all addresses and port 7080 by default, then run the ansible playbook in another shell like this;

	$ ansible-playbook -i hosts test.yml
	PLAY [localhost] **************************************************************************

	TASK [Gathering Facts] ********************************************************************
	ok: [localhost]

	TASK [echo something] *********************************************************************
	changed: [localhost]

	TASK [debug] ******************************************************************************
	ok: [localhost] => {
			"msg": "hello"
	}

	TASK [get current date] *******************************************************************
	changed: [localhost]

	TASK [debug] ******************************************************************************
	ok: [localhost] => {
			"msg": "2018-10-04T15:31:18"
	}

	PLAY RECAP ********************************************************************************
	localhost                  : ok=5    changed=2    unreachable=0    failed=0

